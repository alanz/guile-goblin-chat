(define-module (tests backend)
  #:use-module (goblin-chat backend)
  #:use-module (goblins)
  #:use-module (goblins vat)
  #:use-module (goblins actor-lib methods)
  #:use-module (srfi srfi-64))

(test-begin "test-backend")

(define (make-user vat self-proposed-name)
  "Spawns a user controller pair in vat"
  (define (spawn-controller-pair)
    (spawn-user-controller-pair self-proposed-name))
  (let ((user-controller-pair (vat spawn-controller-pair)))
    (values (car user-controller-pair)
	    (cdr user-controller-pair))))

(define alice-vat (spawn-vat))
(define bob-vat (spawn-vat))
(define mallet-vat (spawn-vat))

(define-values (alice-user alice-controller)
  (make-user alice-vat "Alice"))
(define-values (bob-user bob-controller)
  (make-user bob-vat "Bob"))
(define-values (mallet-user mallet-controller)
  (make-user mallet-vat "Mallet"))

(define chatroom-vat (spawn-vat))
(define chatroom
  (chatroom-vat (lambda _ (spawn ^chatroom "#butterflies"))))

(define alice-channel-vow
  (alice-vat
   (lambda _
     ($ alice-controller 'join-room chatroom))))
(define bob-channel-vow
  (bob-vat
   (lambda _
     ($ bob-controller 'join-room chatroom))))

(define (^print-whatever _bcom user)
  (define (fallback-method . args)
    (pk 'print-whatever 'user-watching user 'args args))

  (define (^hear-room _bcom room channel)
    (lambda (. args)
      (pk 'user user 'from-room room 'args args)))

  (extend-methods
   fallback-method
   ((we-joined-room room channel)
    (<-np channel 'subscribe (spawn ^hear-room room channel)))))

(alice-vat
 (lambda ()
   (define print-whatever
     (spawn ^print-whatever 'alice))
   ($ alice-controller 'connect-client print-whatever)))
(define bob-print-whatever
  (bob-vat
   (lambda ()
     (define print-whatever
       (spawn ^print-whatever 'bob))
     ($ bob-controller 'connect-client print-whatever))))

(alice-vat
 (lambda _
   (<-np alice-channel-vow 'send-message
	 "Hello everyone, how are you?")))
(bob-vat
 (lambda _
   (<-np bob-channel-vow 'send-message
	 "I'm fine, thanks for asking alice.")))
(sleep 3)

(test-end "test-backend")
	     
