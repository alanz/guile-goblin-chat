(use-modules (guix)
             (guix git-download)
             (guix packages)
             (gnu packages autotools)
             (gnu packages guile-xyz)
             (gnu packages pkg-config)
             (gnu packages texinfo))

(define guile-goblins*
  (package
   (inherit guile-goblins)
   (name "guile-goblins")
   (version "0.10-git")
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url "https://gitlab.com/spritely/guile-goblins.git")
                  (commit "2efcc90c26a929a6cbdb5bc34b0883f59f8e70f3")))
            (file-name (string-append name "-" version))
            (sha256
             (base32
              "0c0kprdbpzfg15v2bbw13w58kd24mnblp7444k5d6nxqkga5h3zj"))))
   (native-inputs (list autoconf automake pkg-config texinfo))))

(define packages
  (cons guile-goblins*
        (specifications->packages
         (list "guile" "guile-fibers@1.0.0" "guile-g-golf" "gtk@4"))))

(packages->manifest packages)
