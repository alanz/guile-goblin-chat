# Guile Goblin Chat

This is a demo (but funtional) application which implements a basic secure
chat application ontop of [Guile Goblins](gitlab.com/spritely/guile-goblins).
This also interroperates with the [Racket Goblin Chat](https://gitlab.com/spritely/goblin-chat/-/tree/basic-goblin-chat).

## How to run this

It's easiest to use [GNU Guix](https://guix.gnu.org/) for this, but
you can also use guile packaged on your system if it includes the
necessary dependencies.

For using GNU Guix. It's important you run this with the `--no-grafts`
flag, use these commands to run goblin chat:

``` shell
$ guix shell --pure --no-grafts --manifest=manifest.scm

# To start the client
[inside guix shell]$ guile -L . goblin-chat/onion-client.scm <self-proposed-name> <chatroom sturdyref>

# To connect as a server
[inside guix shell]$ guile -L . goblin-chat/onion-server.scm <user self-proposed-name> <chatroom self-proposed-name>
```

## License

Apache v2, see `LICENSE.txt` file for more information.
