;;; Copyright 2023 Jessica Tallon
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;; http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(define-module (goblin-chat glib-vat)
  #:use-module (ice-9 threads)
  #:use-module (fibers channels)
  #:use-module (goblins core)
  #:use-module (goblins vat)
  #:use-module (g-golf)
  #:use-module (g-golf glib main-event-loop)
  #:use-module (g-golf hl-api glib)
  #:export (make-glib-vat
            spawn-glib-vat
            spawn-gtk-vat
            connect-event))

(define (make-glib-vat)
  (define main-loop (g-main-loop-new))
  (define churn #f)

  (define (start churn-fn)
    (set! churn churn-fn)
    (call-with-new-thread
     (lambda ()
       (g-main-loop-run main-loop))))

  (define (halt)
    (g-idle-add (lambda () (g-main-loop-quit main-loop) #f)))

  (define (send envelope)
    (if (vat-envelope-return? envelope)
        (let ((result-ch (make-channel)))
          (g-idle-add
           (lambda ()
             (put-message result-ch (churn envelope))
             #f))
          (pk 'send 'msg (vat-envelope-message envelope) 'result (get-message result-ch)))
        (g-idle-add
         (lambda ()
           (churn envelope)
           #f))))

  (make-vat #:start start
            #:halt halt
            #:send send))

(define (spawn-glib-vat)
  (let ((vat (make-glib-vat)))
    (vat-start! vat)
    vat))

(define (spawn-gtk-vat)
  (gi-import-by-name "Gtk" "init")
  (let ((vat (spawn-glib-vat)))
    (with-vat vat (gtk-init))
    vat))

;; These are needed because the connect code must run inside a churn
;; this is a hack, there's gotta be a better way.
(define (connect-event obj event thunk)
  (define (^connect-dispatcher _bcom)
    (lambda ()
      (thunk)))
  (define dispatcher (spawn ^connect-dispatcher))
  (connect
   obj event
   (lambda _
     (<-np-extern dispatcher))))
