;;; Copyright 2023 Jessica Tallon
;;; Copyright 2020-2022 Christine Lemmer Webber
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;; http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(use-modules (goblin-chat backend)
             (goblin-chat glib-vat)
             (goblin-chat gui)
             (goblins)
             (goblins vat)
             (goblins ocapn ids)
             (goblins ocapn captp)
             (goblins ocapn netlayer onion)
             (fibers conditions)
             (srfi srfi-1))

(when (<= (length (command-line)) 2)
  (error "No self-proposed name or chatroom specified"))

(define spn (second (command-line)))
(define chatroom-name (third (command-line)))

(define user-vat (spawn-vat))
(define-values (user user-controller)
  (with-vat user-vat (spawn-user-controller-pair spn)))

;; Open the GUI
(define gui-vat (spawn-gtk-vat))
(with-vat
 gui-vat
 (make-main-window user user-controller))


(define chat-vat (spawn-vat))
(define chatroom
  (with-vat chat-vat (spawn ^chatroom (string-append "#" chatroom-name))))

(with-vat
 user-vat
 ($ user-controller 'join-room chatroom))

;; Setup the netlayer
(define onion-vat
  (spawn-vat))
(define-values (onion-netlayer private-key service-id)
  (with-vat onion-vat (new-onion-netlayer)))

(define mycapn
  (with-vat
   onion-vat
   (let* ((mycapn (spawn-mycapn onion-netlayer))
          (chatroom-sref ($ mycapn 'register chatroom 'onion)))
     (display (format #f "Connect on: ~a\n" (ocapn-id->string chatroom-sref)))
     mycapn)))

;; Don't exit
(define forever (make-condition))
(wait forever)
