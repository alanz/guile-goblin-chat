;;; Copyright 2023 Jessica Tallon
;;; Copyright 2020-2022 Christine Lemmer Webber
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;; http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(define-module (goblin-chat backend)
  #:use-module (goblins)
  #:use-module (goblins ghash)
  #:use-module (goblins actor-lib sealers)
  #:use-module (goblins actor-lib common)
  #:use-module (goblins actor-lib methods)
  #:use-module (goblins actor-lib ward)
  #:use-module (goblins actor-lib pubsub)
  #:export (^chatroom
            spawn-user-controller-pair))

(define (^chatroom _bcom self-proposed-name)
  ;; TODO: is ghash the same as ^hasheq?
  (define subscribers
    (spawn ^ghash))

  (define (send-to-subscribers . args)
    ;; TODO: add 'values method to ^ghash.
    (ghash-for-each
     (lambda (_associated-user subscriber)
       (apply <-np subscriber args))
     ($ subscribers 'data)))

  (define (^user-messaging-channel bcom associated-user user-inbox)
    (define dead-beh
      (lambda _
        'CONNECTION-CLOSED))

    (methods
     ((leave)
      ($ subscribers 'remove associated-user)
      (send-to-subscribers 'user-left associated-user)
      (bcom dead-beh))
     ((send-message sealed-msg)
      (send-to-subscribers 'new-message associated-user sealed-msg)
      'OK)
     ((list-users)
      ;; TODO: add 'keys to ^ghash
      (ghash-for-each
       (lambda (user _subscriber)
         user)
       ($ subscribers 'data)))))

  (define (^finalize-subscription bcom associated-user)
    (define (post-finalize-beh . _args)
      (error "Already finalized"))
    (define (pre-finalize-beh user-inbox)
      ;; send to subscribers other than this user first
      (send-to-subscribers 'user-joined associated-user)
      ;; now subscribe this user
      ($ subscribers 'set associated-user user-inbox)
      ;; send to the user who just joined a bunch of messages
      ;; as if everyone *else* already in the channel just joined
      (ghash-for-each
       (lambda (present-user _subscriber)
         (<-np user-inbox 'user-joined present-user))
       ($ subscribers 'data))
      (bcom post-finalize-beh
            (spawn ^user-messaging-channel
                   associated-user user-inbox)))
    pre-finalize-beh)

  (methods
   ((self-proposed-name) self-proposed-name)
   ((subscribe user)
    (define subscription-sealer-vow
      (<- user 'get-subscription-sealer))
    (on subscription-sealer-vow
        (lambda (subscription-sealer)
          (define finalize-sub
            (spawn ^finalize-subscription user))
          (<- subscription-sealer finalize-sub))
        #:promise? #t))))

(define (spawn-user-controller-pair self-proposed-name)
  "Spawn a user and an actor to control the user"
  (define-values (chat-msg-sealer chat-msg-unsealer chat-msg-sealed?)
    (spawn-sealer-triplet))
  (define-values (subscription-sealer subscription-unsealer
                                      subscription-sealed?)
    (spawn-sealer-triplet))
  (define-values (controller-warden controller-incanter)
    (spawn-warding-pair))

  (define rooms->inboxes/channels
    (spawn ^ghash))
  (define client-subscribers
    (spawn ^seteq))

  ;; Here's our user object.  More or less it's a profile that
  ;; provides'a self-proposed name, an unsealer, and a predicate to
  ;; check whether we sealed things.
  (define (^user _bcom)
    (methods
     ((self-proposed-name) self-proposed-name)
     ((get-chat-sealed?) chat-msg-sealed?)
     ((get-chat-unsealer) chat-msg-unsealer)
     ((get-subscription-sealer) subscription-sealer)))
  (define user
    (spawn ^user))

  ;; TODO: is this correct?
  (define (send-to-clients . args)
    (map
     (lambda (client)
       (apply <-np client args))
     ($ client-subscribers 'as-list)))

  (define (^user-inbox bcom context)
    (define room-users
      (spawn ^seteq))
    (define inbox-pubsub
      (spawn ^pubsub))

    (define (revoked-beh . rest)
      (error "Revoked!"))

    (define controller-methods
      (methods
       ((revoke) (bcom revoked-beh))
       ((subscribe subscriber)
        ($ inbox-pubsub 'subscribe subscriber)
        (map
         (lambda (user)
           (<-np subscriber 'user-joined user))
         ($ room-users 'as-list))
        #t)))
    (define public-methods
      (methods
       ((new-message from-user sealed-msg)
        ;; TODO: this should work
        ;; (define chat-unsealer-vow
        ;;   (<- from-user 'get-chat-unsealer))
        ;; (define message-vow
        ;;   (<- chat-unsealer-vow sealed-msg))

        (define message-vow
          (on (<- from-user 'get-chat-unsealer)
              (lambda (chat-unsealer)
                (on (<- chat-unsealer sealed-msg)
                    (lambda (unsealed-message)
                      unsealed-message)
                    #:promise? #t))
              #:promise? #t))

        (on message-vow
            (lambda (message)
              ($ inbox-pubsub 'publish 'new-message
                 context from-user message))))
       ((user-joined user)
        ($ room-users 'add user)
        ($ inbox-pubsub 'publish 'user-joined user))
       ((user-left user)
        ($ room-users 'remove user)
        ($ inbox-pubsub 'publish 'user-left user))
       ((context) context)))

    (ward controller-warden controller-methods
          #:extends public-methods))

  (define (^authenticated-channel _bcom room-channel inbox)
    (extend-methods
     room-channel
     ((send-message contents)
      (on (<- chat-msg-sealer contents)
          (lambda (sealed-msg)
            (<- room-channel 'send-message sealed-msg))))
     ((subscribe subscriber)
      (define (^unsubscribe _bcom)
        (lambda ()
          ($ controller-incanter inbox 'unsubscribe subscriber)))
      ($ controller-incanter inbox 'subscribe subscriber)
      `#(OK ,(spawn ^unsubscribe)))))

  (define (^user-controller _bcom)
    (methods
     ((whoami) user)
     ((connect-client client)
      ($ client-subscribers 'add client)
      `#(OK ,($ rooms->inboxes/channels 'data)))
     ((join-room room)
      (when ($ rooms->inboxes/channels 'has-key? room)
        (error "Already subscribed to the room"))
      (define inbox
        (spawn ^user-inbox room))
      (define sealed-finalizer-vow
        (<- room 'subscribe user))
      ;; First we request to subscribe, unseal it
      (define subscription-finalizer-vow
        (<- subscription-unsealer sealed-finalizer-vow))

      (on (<- subscription-finalizer-vow inbox)
          (lambda (room-channel)
            (define authenticated-ch
              (spawn ^authenticated-channel room-channel inbox))
            ($ rooms->inboxes/channels 'set room authenticated-ch)
            (send-to-clients 'we-joined-room room authenticated-ch)
            authenticated-ch)
          #:promise? #t))))

  (define user-controller
    (spawn ^user-controller))
  (values user user-controller))
