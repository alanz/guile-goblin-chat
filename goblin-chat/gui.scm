;;; Copyright 2023 Jessica Tallon
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;; http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

(define-module (goblin-chat gui)
  #:use-module (goblin-chat backend)
  #:use-module (goblin-chat glib-vat)
  #:use-module (goblins)
  #:use-module (goblins actor-lib ward)
  #:use-module (goblins actor-lib common)
  #:use-module (goblins actor-lib cell)
  #:use-module (goblins actor-lib methods)
  #:use-module (g-golf)
  #:use-module (ice-9 i18n)
  #:export (make-main-window))

;; TODO: Selectively import.
(gi-import "Gtk" #:version "4.0")

(define time-format (locale-time-format))

(define* (^chatroom-messages bcom parent-stack #:key (name #f))
  (define window
    (make <gtk-scrolled-window>))

  (define messages
    (make <gtk-list-box>))
  (set-child window messages)

  (define (make-row)
    (define list-box-row
      (make <gtk-list-box-row>))
    (set-margin-top list-box-row 10)
    (set-margin-start list-box-row 20)
    (set-margin-end list-box-row 20)
    (set-margin-bottom list-box-row 10)
    (append messages list-box-row)

    (define row-box
      (make <gtk-box>
        #:orientation 'vertical))
    (set-child list-box-row row-box)
    (set-hexpand row-box #t)
    row-box)

  (define (add-message from contents ts)
    (define row-box (make-row))
    (define username-box
      (make <gtk-box>
        #:orientation 'horizontal))
    (set-hexpand username-box #t)
    (append row-box username-box)

    (define spn
      (make <gtk-label>))
    (set-hexpand spn #t)
    (append username-box spn)
    (set-xalign spn 0.0)
    (set-markup
     spn
     (format #f "<span foreground=\"blue\" weight=\"bold\">~a</span>" from))

    (define timestamp
      (make <gtk-label>
        #:label (strftime time-format (localtime ts))))
    (append username-box timestamp)

    (define message
      (make <gtk-label>
        #:label contents))
    (set-xalign message 0)
    (append row-box message))

  (define (add-status-message text ts)
    (define row-box (make-row))
    (define box
      (make <gtk-box>
               #:orientation 'horizontal))
    (append row-box box)

    (define gray-message-formatting
      "<span foreground=\"gray\" style=\"italic\">~a</span>")
    (define message
      (make <gtk-label>))
    (set-hexpand message #t)
    (append box message)
    (set-xalign message 0.0)
    (set-markup
     message (format #f gray-message-formatting text))


    (define timestamp
      (make <gtk-label>))
    (define time-as-locale-str
      (strftime time-format (localtime ts)))
    (set-markup
     timestamp
     (format #f gray-message-formatting time-as-locale-str))
    (append box timestamp))


  (methods
   ([set-room-name name]
    (add-titled parent-stack window name name))
    ;;(bcom (^chatroom-messages bcom parent-stack name)))

   ([new-message from message]
    (let ((recieve-time (current-time)))
      (on (<- from 'self-proposed-name)
          (lambda (spn)
            (add-message spn message recieve-time)))))
   ([user-joined user]
    (let ((recieve-time (current-time)))
      (on (<- user 'self-proposed-name)
          (lambda (spn)
            'noop))))
            ;;(add-status-message (format #f "~a joined" spn) recieve-time)))))
   ([user-left user]
    (let ((recieve-time (current-time)))
      (on (<- user 'self-proposed-name)
          (lambda (spn)
            'noop))))))
            ;;(add-status-message (format #f "~a left" spn) recieve-time)))))))

(define (^chatrooms bcom warden window parent-widget)
  (define rooms->stackpages
    (spawn ^ghash))
  (define rooms->channels
    (spawn ^ghash))
  (define names->rooms
    (spawn ^ghash))

  (define box
    (make <gtk-box>
      #:orientation 'horizontal))
  (set-vexpand box #t)
  (set-hexpand box #t)
  (append parent-widget box)

  (define sidebar-stack
    (make <gtk-stack-sidebar>))
  (set-vexpand sidebar-stack #t)
  (append box sidebar-stack)

  (define stack
    (make <gtk-stack>))
  (set-vexpand stack #t)
  (set-hexpand stack #t)
  (set-stack sidebar-stack stack)
  (append box stack)

  (define (^channel-inbox bcom room)
    (methods
     ([user-joined user]
      (let ((page ($ rooms->stackpages 'ref room)))
        ($ page 'user-joined user)))
     ([user-left user]
      (let ((page ($ rooms->stackpages 'ref room)))
        ($ page 'user-left user)))
     ([new-message room from message]
      (let ((page ($ rooms->stackpages 'ref room)))
        ($ page 'new-message from message)))))

  (define beh
    (methods
     ([we-joined-room room channel]
      (define chatroom-messages
        (spawn ^chatroom-messages stack))
      ($ rooms->stackpages 'set room chatroom-messages)
      (define inbox (spawn ^channel-inbox room))
      (<- channel 'subscribe inbox)
      ($ rooms->channels 'set room channel)
      (on (<- room 'self-proposed-name)
          (lambda (channel-spn)
            ($ names->rooms 'set channel-spn room)
            ($ chatroom-messages 'set-room-name channel-spn))))
     ([new-message from msg] (pk 'new-message from msg))))

  (define internal-beh
    (methods
     ([send-message msg]
      (let* ((room-name (get-visible-child-name stack))
             (room ($ names->rooms 'ref room-name))
             (channel ($ rooms->channels 'ref room)))
        (<- channel 'send-message msg)))))

  (ward warden internal-beh
        #:extends beh))


(define (make-main-window user user-controller)
  (define app
    (make <gtk-application>
      #:application-id "goblin-chat.spritely.institute"))

  (define main-window
    (make <gtk-application-window>
      #:title "Goblin Chat"
      #:application app))
  (on (<- user 'self-proposed-name)
      (lambda (spn)
        (set-title main-window (format #f "Goblin Chat - ~a" spn))))

  (define main-box
    (make <gtk-box>
             #:orientation 'vertical
             #:spacing 2))
    (set-child main-window main-box)

  (define-values (chatrooms-warden chatrooms-incanter)
    (spawn-warding-pair))
  (define chatrooms (spawn ^chatrooms chatrooms-warden main-window main-box))

  (<- user-controller 'connect-client chatrooms)

  (define text-entry-box
    (make <gtk-box>
      #:orientation 'horizontal
      #:spacing 10))
  (append main-box text-entry-box)

  (define self-proposed-name-label
    (make <gtk-label>
      #:label ""))
  (append text-entry-box self-proposed-name-label)
  (on (<- user 'self-proposed-name)
      (lambda (spn)
        (set-label self-proposed-name-label (format #f "   ~a" spn))))

  (define message-entry
    (make <gtk-entry>))
  (set-hexpand message-entry #t)
  (append text-entry-box message-entry)
  (define message-entry-buffer (get-buffer message-entry))

  (define (send-message . args)
    (define message (get-text message-entry-buffer))
    (set-text message-entry-buffer "" 0)
    ($ chatrooms-incanter chatrooms 'send-message message))
  (connect-event message-entry 'activate send-message)

  (define message-send-button
    (make <gtk-button>
      #:label "Send!"))
  (append text-entry-box message-send-button)

  (connect-event message-send-button 'clicked send-message)
  (show main-window)
  #f)
